import Drop from 'tether-drop';
import moment from "moment/moment";

angular.module('financier').directive('bulkAddCatTransactions', ($rootScope, $sce, $compile, $timeout, $translate, $filter) => {
  const dateFilter = $filter('date');

  function link(scope, element, attrs, ngModelCtrl) {

    element.on('click', event => {
      event.stopPropagation();

      const template = require('./bulkAddCatTransactions.html');

      const wrap = angular.element('<div></div>').append(template);

      // scope.items = scope.incomes.concat(scope.items);

      const content = $compile(wrap)(scope);

      content.on('keypress keydown', e => {
        if (e.which === 27) {
          // dropInstance.close();
          try {
            if(dropInstance) {
              dropInstance.close();
            }
          }catch (e) {
            console.log("E ", e)
          }
        }
      });

      let dropInstance = new Drop({
        target: element[0],
        content: content[0],
        classes: 'drop-theme-arrows-bounce',
        openOn: 'click',
        position: 'right top',
        constrainToWindow: true,
        constrainToScrollParent: false,
        tetherOptions: {
          targetOffset: '0 -15px',
          optimizations: {
            moveElement: true
          }
        }
      });

      scope.close = () => {
        try {
          if(dropInstance) {
            dropInstance && dropInstance.destroy();
            dropInstance = null;
          }
        }catch (e) {
          console.log("E ", e)
        }

      };

      $rootScope.$broadcast('drop:close');

      scope.$on('drop:close', () => {
        try {
          if(dropInstance) {
            dropInstance.close();
          }
        }catch (e) {
          console.log("E ", e)
        }
      });

      scope.$on('$destroy', () => {
        scope.close();
      });

      dropInstance.on('close', () => {
        $timeout(() => {
          try {
            if(dropInstance){
              dropInstance.destroy();
            }
          }catch (e) {
            console.log("E ", e)
          }


        });
      });

      dropInstance.open();
    });

  }

  function prelink(scope, element, attrs) {
    scope.disableSplit = angular.isDefined(attrs.disableSplit);

    scope.splits = [{
      name: $translate.instant('MULTIPLE_CATEGORIES'),
      id: 'split'
    }];

    scope.incomes = [{
      name: '',
      id: 'income'
    }, {
      name: '',
      id: 'incomeNextMonth'
    }];

    scope.$watch('transactionDate', (newDate, oldDate) => {
      scope.incomes[0].name = $translate.instant('INCOME_FOR', {
        month: dateFilter(scope.transactionDate || new Date(), 'LLLL')
      });

      scope.incomes[1].name = $translate.instant('INCOME_FOR', {
        month: dateFilter(moment(scope.transactionDate || new Date()).add(1, 'month').toDate(), 'LLLL')
      });

      // Don't needlessly trigger upon init
      if (oldDate !== newDate) {
        // Only trigger if income is selected
        if (scope.incomes.indexOf(scope.item) !== -1) {
          scope.$broadcast('autosuggest:updateText');
        }
      }
    });

    // flatten categories
    scope.items = [].concat.apply(
      [],
      Object.keys(scope.masterCategories)
        .sort((a, b) => scope.masterCategories[a].sort - scope.masterCategories[b].sort)
        .map(id => scope.masterCategories[id].categories)
    );

    scope.masterCategoriesArr = Object.keys(scope.masterCategories)
      .map(id => scope.masterCategories[id])
      .sort((a, b) => a.sort - b.sort);

    scope.items = scope.incomes.concat(scope.items);

    if (!scope.disableSplit) {
      scope.items = scope.splits.concat(scope.items);
    }

    let setByParent = false,
      firstRun = true;

    scope.$watch('ngModel', (ngModel, oldNgModel) => {
      for (let i = 0; i < scope.items.length; i++) {
        if (scope.items[i].id === scope.ngModel) {
          if (!scope.item || scope.ngModel !== scope.item.id) {
            if (!firstRun) {
              setByParent = true;
            } else {
              firstRun = true;
            }
          }

          scope.item = scope.items[i];
        }
      }
    });

    scope.$watch('item', (newItem, oldItem) => {

      if (newItem !== oldItem) {
        if (scope.ngModel !== newItem.id) {
          setByParent = false;
        }

        scope.ngModel = newItem.id;
      }
    });

    scope.categoryFilter = (item, searchInput, pristineInputField) => {
      if (pristineInputField || setByParent) {
        return true;
      }

      const searchInputLower = searchInput.toLowerCase();

      for (let id in scope.masterCategories) {
        if (scope.masterCategories.hasOwnProperty(id) && scope.masterCategories[id].categories.indexOf(name.id || name) !== -1) {
          if (scope.masterCategories[id].name && scope.masterCategories[id].name.toLowerCase().indexOf(searchInputLower) !== -1) {
            return true;
          }
        }
      }

      if (angular.isString(item)) {
        return scope.categories[item] && scope.categories[item].name.toLowerCase().indexOf(searchInputLower) !== -1;
      }

      return item.name && item.name.toLowerCase().indexOf(searchInputLower) !== -1;

    };

    scope.getCategoryBalance = (categoryId, date) => {
      const month = scope.manager.getMonth(date || new Date());

      const categoryCache = month.categoryCache[categoryId];

      if (categoryCache) {
        return month.categoryCache[categoryId].balance;
      }

      return 0;
    };

    scope.onSubmit = () => {
      //console.log("onSubmit directive");
      // $rootScope.$broadcast('transaction:memo:focus', { index: scope.$parent.splitIndex });
    };
    
  }

  return {
    restrict: 'A',
    controller: 'bulkAddCatTransactionsCtrl as bulkAddCatTransactionsCtrl',
    scope: {
      bulkAddCatTransactions: '=',
      masterCategories: '=',
      categories: '=',
      manager: '=',
      transactionDate: '=',
      payees: '=',
      stopPropagation: '='
    },
    // link
    compile: function (tElem, tAttrs) {
      //console.log(': compile');
      //console.log(tElem.html());
      return {
        pre: prelink,
        post: link
      }
    }
  };
});
