import moment from 'moment';

angular.module('financier').controller('importTransactionsCtrl', function($locale, myAccount, manager, myBudg, transaction, $q, $rootScope, $scope, $stateParams, category, masterCategory, addCategory, masterCategories, MonthCategory, month, currencyDigits, filterAccounts, onBudgetAccounts, offBudgetAccounts, $translate) {
  const Transaction = transaction($stateParams.budgetId);
  const Category = category($stateParams.budgetId);
  const MasterCategory = masterCategory($stateParams.budgetId);
  const Month = month($stateParams.budgetId);

  this.DECIMAL_SEP = $locale.NUMBER_FORMATS.DECIMAL_SEP;

  //this.account = myAccount;

  // this.startingBalanceDate = new Date();

  this.getGroupName = onBudget => $translate.instant(onBudget ? 'ON_BUDGET' : 'OFF_BUDGET');



  this.submit = file => {

    var reader = new FileReader();

    reader.onload = (theFile => {
      // const promises = [
      //   //myBudg.put(myAccount)
      // ];
      return e => {
        let docs;
        const promises = [
          //myBudg.put(myAccount)
        ];
        try {
          docs = e.target.result.split("\n");
        } catch(e) {
          this.error = e;
          throw e; // rethrow for debugging
        }

        let newTransaction, amount, row, tdate;
        var arrayLength = docs.length;
        //var transactions = [];
        // console.log("Doc");
        // console.log(docs);
        for (var i = 1; i < arrayLength; i++) {
          row = docs[i].trim().replace(/\"/g, '').split(",");

          if( row.length < 4){
            continue;
          }
          if(row[3].trim() == ""){
            amount =  -1 * parseFloat(row[4]) * 100;
          }else {
            amount =  100 * parseFloat(row[3]);
          }

          tdate = moment(row[0], 'DD/MM/YYYY').format('YYYY-MM-DD');
          newTransaction = new Transaction({
            value: amount,
            memo: row[1],
            date: tdate,
            account: myAccount
          }, myBudg.put);

          // console.log("**** Details \t", i, " ****");
          // console.log(row);
          // console.log(myAccount);
          // console.log(tdate,"date", row[0]);
          // console.log(amount,"in", row[3],"out", row[4]);
          // console.log(row[1]);


          // if(manager){
          //   console.log("Manager YES");
          // }else{
          //   console.log("Manager NO");
          // }
          //transactions.push(newTransaction);

          //Do something
          manager.addTransaction(newTransaction);

          promises.push(myBudg.put(newTransaction));


        }
        //console.log("Transactions", transactions);
        //manager.addTransaction(transactions);
        $q.all(promises).then(() => {
          $scope.closeThisDialog();
        });
      };
    })(file);

    // Read in the image file as a data URL.
    reader.readAsText(file);


    // $q.all(promises).then(() => {
    //   $scope.closeThisDialog();
    // });

  };

});
