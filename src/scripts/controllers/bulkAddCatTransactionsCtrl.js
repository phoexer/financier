angular.module('financier').controller('bulkAddCatTransactionsCtrl', function ($rootScope, $scope, ngDialog) {
  // let {manager, categories, masterCategories, payees} = data;

  this.addIncome = (transactions, event, income) => {
    // console.log("onIncome controller");
    // console.log("cat_id", income);
    // console.log("transactions", transactions);
    // console.log("$scope", $scope);

    // Remove the emit change handle to prevent 409 conflicts
    const saveFn = $scope.manager.saveFn;

    // $scope.stopPropagation(event);


    transactions.forEach(transaction => {
      transaction.fn = null;

      transaction.category = income.id;

      // Save transaction
      transaction.fn = saveFn;
      transaction._emitChange();
      // $scope.manager.removeTransaction(transaction);
      // $scope.manager.addTransaction(transaction);

    });
  }
  this.addCat = (transactions, event, cat) => {
    // console.log("onSubmit controller");
    // console.log("cat_id", cat);
    // console.log("transactions", transactions);
    // console.log("$scope", $scope);

    // Remove the emit change handle to prevent 409 conflicts
    const saveFn = $scope.manager.saveFn;

    // $scope.stopPropagation(event);


    transactions.forEach(transaction => {
      transaction.fn = null;

      transaction.category = cat.id;

      // Save transaction
      transaction.fn = saveFn;
      transaction._emitChange();
      // $scope.manager.removeTransaction(transaction);
      // $scope.manager.addTransaction(transaction);

    });

    // $scope.close();
    // $rootScope.$broadcast('account:deselectTransactions');

  };

// this.removeAll = (transactions, event) => {
//   $scope.stopPropagation(event);
//
//
//
//   for (let i = 0; i < transactions.length; i++) {
//     if (transactions[i].transfer && transactions[i].transfer.constructorName === 'SplitTransaction') {
//       return ngDialog.open({
//         template: require('../../views/modal/noDeleteSplitTransfer.html'),
//         controller: 'cancelClickCtrl'
//       });
//     }
//   }
//
//   const reconciled = transactions.reduce((prev, curr) => {
//     return prev + (curr.reconciled ? 1 : 0);
//   }, 0);
//
//   if (reconciled) {
//     const scope = $rootScope.$new({});
//     scope.reconciled = reconciled;
//     scope.length = transactions.length;
//     scope.stopPropagation = $scope.stopPropagation;
//
//     ngDialog.openConfirm({
//       template: require('../../views/modal/removeTransactionsConfirm.html'),
//       scope,
//       className: 'ngdialog-theme-default ngdialog-theme-default--danger modal'
//     })
//       .then(remove);
//   } else {
//     remove();
//   }
//
//   function remove() {
//     transactions.forEach(transaction => {
//       $scope.manager.removeTransaction(transaction);
//
//       if (transaction.payee) {
//         removePayee(transaction);
//       }
//
//       transaction.remove();
//     });
//
//     $scope.close();
//     $rootScope.$broadcast('account:deselectTransactions');
//   }
// };

// function removePayee(transaction) {
//   const transactions = Object.keys($scope.manager.transactions).map(k => $scope.manager.transactions[k]);
//
//   for (let i = 0; i < transactions.length; i++) {
//     if (transactions[i].payee === transaction.payee &&
//       transactions[i] !== transaction) {
//       return;
//     }
//   }
//
//   if ($scope.payees[transaction.payee] && !$scope.payees[transaction.payee].internal) {
//     $scope.payees[transaction.payee].remove();
//     delete $scope.payees[transaction.payee];
//   }
// }
})
;
